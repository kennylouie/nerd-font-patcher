# Nerd Font Patcher Docker Image

## Intro

Dockerfile to build the latest (at time of work) nerd font patcher to patch any font.

## Usage

```
docker pull kennylouie/nerd-font-patcher:latest
```
