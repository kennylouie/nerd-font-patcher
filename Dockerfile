FROM debian:stable-20201209-slim

WORKDIR /nerd-fonts

ENV FONTFORGE_VERSION=20201107 \
	NERDFONTS_VERSION=2.1.0

# Install dependencies
RUN apt update && \
	apt install -y \
		wget libjpeg-dev libtiff5-dev \
		libpng-dev libfreetype6-dev libgif-dev libgtk-3-dev \
		libxml2-dev libpango1.0-dev libcairo2-dev libspiro-dev \
		libuninameslist-dev python3-dev ninja-build cmake \
		build-essential gettext

# Install fontforge
RUN TMP=$(mktemp -d) && \
	cd ${TMP} && \
	wget https://github.com/fontforge/fontforge/releases/download/${FONTFORGE_VERSION}/fontforge-${FONTFORGE_VERSION}.tar.xz && \
	tar -axf fontforge-${FONTFORGE_VERSION}.tar.xz && \
	cd fontforge-${FONTFORGE_VERSION} && \
	mkdir build && cd build && \
	cmake -GNinja .. && \
	ninja && \
	ninja install && \
	rm -rf ${TMP}

# Install nerd-fonts
RUN wget https://github.com/ryanoasis/nerd-fonts/archive/v${NERDFONTS_VERSION}.tar.gz && \
	tar -axf v${NERDFONTS_VERSION}.tar.gz --strip-components 1 && \
	rm v${NERDFONTS_VERSION}.tar.gz

ENTRYPOINT ["fontforge", "--script", "/nerd-fonts/font-patcher"]
